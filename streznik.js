var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  
  
  	
  	
  	if(req.query.naslov == "") res.send( "Napaka pri dodajanju novice!");
  	else if(req.query.povzetek== "") res.send( "Napaka pri dodajanju novice!");
  	else if(req.query.kategorija== "") res.send( "Napaka pri dodajanju novice!");
  	else if(req.query.postnaStevilka== "") res.send( "Napaka pri dodajanju novice!");
  	else if(req.query.kraj== "") res.send( "Napaka pri dodajanju novice!");
  	else if(req.query.povezava== "") res.send( "Napaka pri dodajanju novice!");
  	
	
	  var id = 0;
	  
	  for(var i = 0; i<noviceSpomin.length; i++)
	  {
	    if(noviceSpomin[i].id >= id) id = noviceSpomin[i].id ;
	  }
	  
	 
	  	noviceSpomin.push({"id": id + 1 ,"naslov": req.query.naslov,"povzetek":req.query.povzetek,"kategorija":req.query.kategorija,"postnaStevilka":req.query.postnaStevilka,"kraj":req.query.kraj,"povezava":req.query.povezava});
	  	res.redirect("/seznam.html");
	  	
	  console.log(req.query);
  
  
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
  
  
		var alijenajdena = false;
	
		for(var i =0; i<noviceSpomin.length; i++)
		{
		if(noviceSpomin[i].id == req.query.id)
		{
		   alijenajdena = true;
			 noviceSpomin.splice(i, 1);
		}
		}
	  
	  if(req.query.id == null)  res.redirect("Napačna zahteva!");
		if(alijenajdena) res.redirect("/seznam.html");
		else res.send( "Novica z id-jem " + req.query.id + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		
		
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
